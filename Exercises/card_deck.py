#define possible values
colors = ["trefla","inima neagra","caro","inima rosie"]
values = range(2,15)
deck = []
#build the deck
for value in values:
    for color in colors:
        card = {}
        card["value"]=value
        card["color"]=color
        deck.append(card)
#there should be 52 cards
print len(deck)

def show_card(card):
    """
    :param card:
    :return:
    """
    if card:
        print "%s de %s" %(card["value"],card["color"])

def compare(c1,c2):
    """
    :param c1: card
    :param c2: card
    :return: card - higher value
    """
    if c1["value"] > c2["value"]:
        return c1
    elif c1["value"] < c2["value"]:
        return c2
    else:
        print "Cartile sunt egale"
        return None

#Let's try it out
import random
mine = random.choice(deck)
print "Mine is : ", show_card(mine)
yours = random.choice(deck)
print "Yours is: ", show_card(yours)
print "The winner is: ", show_card(compare(mine,yours))