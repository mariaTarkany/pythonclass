__author__ = 'student'

from rgkit import rg

class Robot():
    # def __init__(self, name, color, height, weight):
    #     self.name = name
    #     self.color = color
    #     self.height = height
    #     self.weight = weight
    def act(self, game):
        if self.location <> rg.CENTER_POINT:
            return ['move', rg.toward(self.location, rg.CENTER_POINT)]
        return ['suicide']