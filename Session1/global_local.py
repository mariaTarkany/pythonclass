#global scope

a = 55


def div_by_five(x):
    # local scope
    a = x / 5
    print "Local var ", a

print "Global var ", a
div_by_five(a)
print "Global var ", a
