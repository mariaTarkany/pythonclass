__author__ = 'student'

from rgkit import rg
from Log import Logger

class Robot(Logger):
    def __init__(self, name, color, height, weight):
        super(Robot,self).__init__("logRobot.txt")
        self.name = name
        self.color = color
        self.height = height
        self.weight = weight
    def act(self, game):
        # self.error("Robot - debug - Name:%s, color: %s " %(self.name, self.color))
        if self.location <> rg.CENTER_POINT:
            return ['move', rg.toward(self.location, rg.CENTER_POINT)]
        return ['suicide']



