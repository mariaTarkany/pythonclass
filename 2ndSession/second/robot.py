__author__ = 'student'

from logs import *

class Robot():
    def __init__(self, name, color, height, weight):
        self.name = name
        self.color = color
        self.height = height
        self.weight = weight

    def print_name(self):
        logging.info("We are in print_name")
        return self.name

    def print_color(self):
        logging.basicConfig(filename="mlogs.log", level=logging.DEBUG)
        logging.debug("This will be in the log file")
        return self.color

    def print_dimension(self):
        # logging.basicConfig(filename="mlogs.log", level=logging.INFO,format="%(asctime)s %(message)s)" # include timestamp)
        # logging.debug("This will be in the log file")
        return self.height + " x " + self.weight

    def print_all_info(self):
        print self.print_name() + " " + self.print_color() + " " + self.print_dimension()